import lodashGet from 'lodash/get';
import englishMessages from './en';

let messages = englishMessages;

let locale = 'en';

const i18nProvider = {
    translate: key => lodashGet(messages, key),
    changeLocale: newLocale => {
        messages = (newLocale === 'en') ? englishMessages : englishMessages;
        locale = newLocale;
        return Promise.resolve();
    },
    getLocale: () => locale,
    allowMissing: true,
};

export default i18nProvider;