import { fetchUtils } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';
import addUploadFeature from './addUploadCapabilities';

const apiUrl = process.env.REACT_APP_API_URL;
const apiUsername = process.env.REACT_APP_API_USERNAME;
const apiPassword = process.env.REACT_APP_API_PASSWORD;

const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    // Authorization by Basic Auth
    const basicAuth = 'Basic ' + btoa(apiUsername + ':' + apiPassword);
    options.headers.set('Authorization', basicAuth);
    /* options.user = {
        authenticated: true,
        token: 'SRTRDFVESGNJYTUKTYTHRG'
    }; */
    return fetchUtils.fetchJson(url, options);
};
const dataProvider = jsonServerProvider(apiUrl, httpClient);

// upload image capable
const uploadCapableDataProvider = addUploadFeature(dataProvider);
// eslint-disable-next-line
const delayedDataProvider = (type, resource, params) =>
    new Promise(resolve =>
        setTimeout(
            () => resolve(uploadCapableDataProvider(type, resource, params)),
            1000
        )
    );

export default dataProvider;