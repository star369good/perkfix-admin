import React from 'react';
import { Button, SaveButton, Toolbar } from 'react-admin';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    toolbar: {
        backgroundColor: theme.palette.background.paper,
        marginBottom: 30,
        marginRight: '15%',
    },
    button: {
        textTransform: 'initial',
        boxShadow: 'inset 0px 1px 0px 0px #ffffff',
        background: 'linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%)',
        backgroundColor: '#f9f9f9',
        borderRadius: 6,
        border: '1px solid #dcdcdc',
        fontWeight: 'bold',
        fontSize: '1.2em',
    },
    buttonCancel: {
        color: '#000',
        marginLeft: 'auto',
    },
    buttonSave: {
        color: 'rgba(0, 145, 255, 1)',
        marginLeft: 8,
        paddingLeft: 30,
        paddingRight: 30,
    }
}));

export default (props) => {
    const classes = useStyles();
    const { onSubmit, onCancel, cancelButtonLabel, saveButtonLabel, ...other } = props;
    return (
        <Toolbar {...other}
            key="toolbar"
            className={classes.toolbar}
            >
            <Button
                key="cancelButton"
                label={cancelButtonLabel || "ra.action.cancel"}
                redirect={false}
                submitOnEnter={false}
                variant="contained"
                size="large"
                className={`${classes.button} ${classes.buttonCancel}`}
                onClick={onCancel}
            />
            <SaveButton
                key="saveButton"
                label={saveButtonLabel || "ra.action.save"}
                redirect={false}
                submitOnEnter={true}
                size="large"
                className={`${classes.button} ${classes.buttonSave}`}
                icon={(<></>)}
                onClick={onSubmit}
            />
        </Toolbar>
    );
}