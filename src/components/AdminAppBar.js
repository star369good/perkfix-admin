import React from 'react';
import { toggleSidebar } from 'ra-core';
import { useDispatch } from 'react-redux';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: 50
  },
  appBar: {
    backgroundColor: '#4d4d4d',
    height: 100,
    justifyContent:'center'
  },
  logoutButton: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(4),
    backgroundColor: '#00c888'
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    fontSize: 30,
    marginLeft: theme.spacing(5)
  },
}));

const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}))(Badge);

const AdminAppBar = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton className={classes.menuButton} color="inherit" aria-label="menu" onClick={() => dispatch(toggleSidebar())}>
            <img src={process.env.PUBLIC_URL + '/images/perkfix-green-logo-pf.png'} alt="logo" width="25"/>
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            People Ops
          </Typography>
          <Typography className={classes.menuButton}>
              Welcome back, Amy.
          </Typography>
          <Button color="inherit" variant="contained" className={classes.logoutButton}>
            Log Out
          </Button>
          <Badge badgeContent={"!"} color="error" className={classes.menuButton}>
            <MailIcon />
          </Badge>
          <IconButton className={classes.menuButton}>
            <StyledBadge
                overlap="circle"
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
                }}
                variant="dot"
            >
                <Avatar alt="Remy Sharp" src={process.env.PUBLIC_URL + '/images/avatar/1.png'} />
            </StyledBadge>
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default AdminAppBar;