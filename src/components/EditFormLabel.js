import React from 'react';
import { FormLabel } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const StyledLabel = withStyles({
    root: {
        color: '#000',
        fontSize: '1.2em',
        fontWeight: 'bold',
        paddingLeft: 20,
    },
    asterisk: {
        color: "red"
    },
})(FormLabel);

export default (props) => (
    <StyledLabel 
        {...props} 
    >
        {props.children}
    </StyledLabel>
);