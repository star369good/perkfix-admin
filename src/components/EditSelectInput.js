import React from 'react';
import { SelectInput } from 'react-admin';

export default (props) => (
    <SelectInput 
        {...props} 
        source={props.source} 
        variant="outlined" 
        label="" 
    />
);