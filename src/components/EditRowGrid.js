import React from 'react';
import Grid from '@material-ui/core/Grid';

export default ({basePath, ...props}) => (
    <Grid container spacing={2} alignItems="center" style={{ width : '100%' }} >
        {props.children}
    </Grid>
);