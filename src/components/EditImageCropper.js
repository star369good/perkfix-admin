/* eslint-disable jsx-a11y/media-has-caption, class-methods-use-this */
import React, { Fragment, PureComponent } from 'react';
import ReactDOM from 'react-dom'; // eslint-disable-line
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Button } from "react-admin";
import CloudUpload from '@material-ui/icons/CloudUpload';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import DesktopMacIcon from '@material-ui/icons/DesktopMac';
import ShopIcon from '@material-ui/icons/Shop';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    button: {
        textTransform: 'initial',
        boxShadow: 'inset 0px 1px 0px 0px #ffffff',
        background: 'linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%)',
        backgroundColor: '#f9f9f9',
        borderRadius: 6,
        border: '1px solid #dcdcdc',
        fontWeight: 'bold',
        fontSize: '1.2em',
    },
    buttonCancel: {
        color: '#000',
    },
    buttonSave: {
        color: 'rgba(0, 145, 255, 1)',
        marginLeft: 30
    }
}));


const EditDialog = (props) => {
    const classes = useStyles();
    const { title, showDialog, handleSaveClick, handleCloseClick, dialogRef, onSelectFile } = props;
    const fileRef = React.createRef();
    const [ currentFile, setFile ] = React.useState(null);

    return (
      <Fragment>
        <Dialog
          fullWidth
          open={showDialog}
          onClose={handleCloseClick}
          aria-label={title}
          ref={dialogRef}
        >
          <DialogTitle>{title}</DialogTitle>
          <DialogContent style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-around', margin: 30 }}>
              <DesktopMacIcon 
                style={{ fontSize: 50, cursor: 'pointer' }} />
              <input type='file' id='file' ref={fileRef} style={{ opacity:'0', position: "absolute", top: "107px", left: "151px", cursor: "pointer", zIndex: "10", bottom: "149px", width: "47px"}} onChange={e => {  if (e.target.files && e.target.files.length > 0) setFile(e.target.files[0]);  }} />
              <ShopIcon style={{ fontSize: 50 }} />
          </DialogContent>
          <DialogActions style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', margin: 20 }}>
            <Button 
                label="ra.action.cancel"
                onClick={handleCloseClick}
                variant="contained"
                size="large"
                className={`${classes.button} ${classes.buttonCancel}`} >
            </Button>
            <Button 
                label="ra.action.upload" 
                onClick={() => { onSelectFile(currentFile); handleSaveClick(); }}
                variant="contained"
                size="large"
                className={`${classes.button} ${classes.buttonSave}`} >
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
};


/**
 * Load the image in the crop editor.
 */
// const cropEditor = document.querySelector('#crop-editor');

class EditImageCropper extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
        src: null,
        crop: {
        //   x: 200,
        //   y: 200,
        //   unit: '%',
        //   width: 30,
        //   aspect: 16 / 9,
        },
        showDialog: false,
    };
   this.onChangePhtoClick.bind(this);
  }

  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      this.readFile(e.target.files[0]);
    }
  };

  readFile = file => {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.setState({ src: reader.result });
    });
    reader.readAsDataURL(file);
  };

  onImageLoaded = image => {
    console.log('onImageLoaded');
    // this.imageRef = image;
    // this.setState({ crop: { unit: 'px', width: 50, height: 50 } });
    // return false;
  };

  onCropComplete = (crop, percentCrop) => {
    console.log('onCropComplete', crop, percentCrop);
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    // console.log('onCropChange', crop, percentCrop);
    this.setState({ crop: percentCrop });
  };

  onDragStart = () => {
    console.log('onDragStart');
  };

  onDragEnd = () => {
    console.log('onDragEnd');
  };

  onChangeToIncompleteCropClick = () => {
    this.setState({
      crop: {
        aspect: 16 / 9,
        unit: '%',
        width: 100,
      },
    });
  };

  onChangePhtoClick = e => {
      e.preventDefault();
      console.log("change photo clicked");
      this.setState({ showDialog: true });
  };

  handleSaveClick = () => {
    this.setState({ showDialog: false });
  }

  handleCloseClick = () => {
    this.setState({ showDialog: false });
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    return new Promise(resolve => {
      canvas.toBlob(blob => {
        blob.name = fileName; // eslint-disable-line no-param-reassign
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
      }, 'image/jpeg');
    });
  }

  makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      this.getCroppedImg(this.imageRef, crop, 'newFile.jpeg').then(croppedImageUrl =>
        this.setState({ croppedImageUrl })
      );
    }
  }

  renderVideo = (mp4Url) => (
    <video
      autoPlay
      loop
      style={{ display: 'block', maxWidth: '100%' }}
      onLoadStart={e => {
        // You must inform ReactCrop when your media has loaded.
        e.target.dispatchEvent(new Event('medialoaded', { bubbles: true }));
      }}
    >
      <source src={mp4Url} type="video/mp4" />
    </video>
  );

  renderSelectionAddon = () => <input placeholder="Type something" />;

  render() {
    const { croppedImageUrl, showDialog } = this.state;

    return (
      <div className="" style={{ width: '150px' }}>
        {/*<div>
          <input type="file" onChange={this.onSelectFile} />
        </div> */}
        <EditDialog 
            title="Upload photo" 
            submit={(result) => {console.log(result)}} 
            handleSaveClick={this.handleSaveClick} 
            handleCloseClick={this.handleCloseClick} 
            showDialog={showDialog}
            onSelectFile={this.readFile} />
            {!this.state.src && (
                <div style={{ width: '150px', height: '200px', backgroundColor: '#ececec', display: 'flex', alignItems: 'center', justifyContent: 'center', cursor: 'pointer', flexDirection: 'column' }} 
                    onClick={this.onChangePhtoClick} >
                        <CloudUpload />
                        <h5>Upload Photo</h5>
                </div>
            )}
            {this.state.src && (
              <>
                <ReactCrop
                  // renderComponent={this.renderVideo()}
                  src={this.state.src}
                  crop={this.state.crop}
                  ruleOfThirds
                  // circularCrop
                  onImageLoaded={this.onImageLoaded}
                  onComplete={this.onCropComplete}
                  onChange={this.onCropChange}
                  onDragStart={this.onDragStart}
                  onDragEnd={this.onDragEnd}
                  // renderSelectionAddon={this.renderSelectionAddon}
                  // maxWidth={100}
                  // minHeight={100}
                />
                {/* <button onClick={this.onChangeToIncompleteCropClick}>Change to incomplete aspect crop</button> */}
                <a href="/" onClick={this.onChangePhtoClick} >
                    Change Photo
                </a>
              </>
            )}
        {croppedImageUrl && <img alt="Crop" src={croppedImageUrl} />}
      </div>
    );
  }
}

export default EditImageCropper;