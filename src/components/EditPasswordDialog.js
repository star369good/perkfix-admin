import React, { Fragment, PureComponent } from 'react';
import clsx from 'clsx';
import { Button } from "react-admin";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import FormControl from '@material-ui/core/FormControl'
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import { makeStyles } from '@material-ui/core/styles';
import PasswordStrengthMeter from './PasswordStrengthMeter';

const useStyles = makeStyles((theme) => ({
    button: {
        textTransform: 'initial',
        boxShadow: 'inset 0px 1px 0px 0px #ffffff',
        background: 'linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%)',
        backgroundColor: '#f9f9f9',
        borderRadius: 6,
        border: '1px solid #dcdcdc',
        fontWeight: 'bold',
        fontSize: '1.2em',
    },
    buttonCancel: {
        color: '#000',
    },
    buttonSave: {
        color: 'rgba(0, 145, 255, 1)',
        marginLeft: 30
    }
}));


const EditPasswordDialog = (props) => {
    const classes = useStyles();
    const { title, showDialog, handleSaveClick, handleCloseClick, dialogRef } = props;
    const [values, setValues] = React.useState({
      password: '',
      confirm: '',
      showPassword: false,
      showConfirmPassword: false
    });
  
    const handleChange = (prop) => (event) => {
      setValues({ ...values, [prop]: event.target.value });
    };
  
    const handleClickShowPassword = () => {
      setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleClickShowConfirmPassword = () => {
      setValues({ ...values, showConfirmPassword: !values.showConfirmPassword });
    };
  
    const handleMouseDownPassword = (event) => {
      event.preventDefault();
    };
   

    return (
      <Fragment>
        <Dialog
          fullWidth
          open={showDialog}
          onClose={handleCloseClick}
          aria-label={title}
          ref={dialogRef}
        >
          <DialogTitle><PasswordStrengthMeter password={values.password}/></DialogTitle>
          <DialogContent style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-around', margin: 30 }}>
              <div style={{width:"80%", background:"rgba(124, 130, 139, 0.83)", color: "white", borderRadius: "3px", textAlign: "center", paddingBottom: "50px"}}>
                <p>Create password for your Perfix.com People Ops platform</p>
                <FormControl className={clsx(classes.margin, classes.textField)}>
                  <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                  <Input
                    require="true"
                    id="standard-adornment-password"
                    type={values.showPassword ? 'text' : 'password'}
                    value={values.password}
                    onChange={handleChange('password')}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {values.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
                <FormControl className={clsx(classes.margin, classes.textField)}>
                  <InputLabel htmlFor="standard-adornment-password">Confirm</InputLabel>
                  <Input
                    require="true"
                    id="standard-adornment-password"
                    type={values.showConfirmPassword ? 'text' : 'password'}
                    value={values.confirm}
                    onChange={handleChange('confirm')}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowConfirmPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {values.showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>
          </DialogContent>
          <DialogActions style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginBottom: 20 }}>
            <Button 
                label="ra.action.cancel"
                onClick={handleCloseClick}
                variant="contained"
                size="large"
                className={`${classes.button} ${classes.buttonCancel}`} >
            </Button>
            <Button 
                label="ra.action.save" 
                // onClick={() => { onSelectFile(currentFile); handleSaveClick(); }}
                variant="contained"
                size="large"
                className={`${classes.button} ${classes.buttonSave}`} >
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
};

export default EditPasswordDialog;