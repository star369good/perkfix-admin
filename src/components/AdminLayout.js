import React from 'react';
import { Layout, Notification } from 'react-admin';
import { AdminAppBar, AdminMenu } from './';

const AdminLayout = (props) => {
    return (<Layout
        {...props}
        appBar={AdminAppBar}
        menu={AdminMenu}
        notification={Notification}
    />);
};

export default AdminLayout;