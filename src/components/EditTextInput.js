import React from 'react';
import { TextInput } from 'react-admin';

export default (props) => (
    <TextInput 
        {...props} 
        source={props.source} 
        variant="outlined" 
        placeholder={props.placeholder} 
        label="" 
    />
);