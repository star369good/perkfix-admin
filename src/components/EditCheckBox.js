import React, { Component } from 'react'
import { Checkbox, withStyles } from '@material-ui/core';
import Check from "@material-ui/icons/Check";

const CheckBoxStyle = theme => ({
    container: {
        display: 'inline-block',
        marginLeft: "17px"
    },
    label: {
        color: '#000',
        fontSize: '1.2em',
        fontWeight: 'bold',
        marginLeft: "10px" 
    },
    checked: {
        color: '#A30C0C',
        fill: '#D8D8D8',
        backgroundColor: '#D8D8D8',
        '&:hover' : {
            backgroundColor: '#D8D8D8'
        }
    },
    root: {
        border: '1px solid #707070',
        fill: '#D8D8D8',
    },
    checkedIcon: {
        width: "20px",
        height: "20px",
        border: "1px solid #707070",
        backgroundColor: '#D8D8D8',
        borderRadius: "4px",
    },
    uncheckedIcon: {
        width: "0px",
        height: "0px",
        padding: '10px 10px',
        border: "1px solid #707070",
        backgroundColor: '#D8D8D8',
        borderRadius: "4px",
    },
    asterisk: {
        color: "red"
    },
});

class EditCheckBox extends Component
{
    render(){
        const { classes, label, className, children, ...rest } = this.props;
        return(
            <div className={ `${className} ${classes.container}` } >
                <Checkbox
                    { ...rest}
                    checkedIcon={<Check className={classes.checkedIcon} />}
                    icon={<Check className={classes.uncheckedIcon} />}
                    disableRipple
                />
                <span className={ `${classes.label}` }>{ children }</span>
            </div>
        )
    }
}

export default withStyles(CheckBoxStyle)(EditCheckBox)