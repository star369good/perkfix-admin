import React, { createElement } from 'react';
import { useSelector } from 'react-redux';
import { MenuItemLink, getResources } from 'react-admin';
import { withRouter } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    menuItem: {
        margintTop: 5,
        marginBottom: 5,
        color: '#000'
    },
    activeClass: {
        backgroundColor: '#868686',
        color: '#fff'
    },
    programDivider: {
        marginTop: 50
    },
    settingDivider: {
        marginTop: 100
    }
}));

const ProgramDivider = (props) => (
    <div className={props.classes.programDivider}>
        { props.open ?
        (<>
        <hr/>
        <h3 style={{ paddingLeft : 20, marginBottom: 0 }}>Program</h3>
        <h3 style={{ paddingLeft : 20, marginTop: 0 }}>Management</h3>
        </>)
        : 
        <hr/> }
    </div>
);

const SettingDivider = (props) => (
    <hr  className={props.classes.settingDivider}/>
);

const AdminMenu = ({ onMenuClick, logout }) => {
    const classes = useStyles();
    const open = useSelector(state => state.admin.ui.sidebarOpen);
    const resources = useSelector(getResources);
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('sm'));

    return (
        <div>
            {resources.map(resource => {
                const customMenuItem = (
                    <MenuItemLink
                        key={resource.name}
                        to={`/${resource.name}${(resource.options && resource.options.url) || ''}`}
                        primaryText={(resource.options && resource.options.label) || resource.name}
                        leftIcon={createElement(resource.icon)}
                        onClick={onMenuClick}
                        sidebarIsOpen={open}
                        className={classes.menuItem}
                        activeClassName={classes.activeClass}
                    />
                );
                if (resource.name === "perk" && matches) {
                    return [(<ProgramDivider key="programdriver" classes={classes} open={open}/>), customMenuItem];
                }
                else if (resource.name === "setting" && matches) {
                    return [(<SettingDivider key="settingdrvier" classes={classes} open={open}/>), customMenuItem];
                }
                return customMenuItem;
            })}
        </div>
    );
}

export default withRouter(AdminMenu);