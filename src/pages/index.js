import { Automation } from './automation';
import { Faq } from './faq';
import { Home } from './home';
import { Inbox } from './inbox';
import { MyOrgEdit } from './myOrg';
import { Perks } from './perks';
import { Reports } from './reports';
import { Settings } from './settings';
import { Setup } from './setup';
import { Teams } from './teams';
import { Tools } from './tools';
import NoFound from './NoFound';

export { Automation, Faq, Home, Inbox, MyOrgEdit, NoFound, Perks, Reports, Settings, Setup, Teams, Tools };