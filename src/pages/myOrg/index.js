import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

import { OrgInfoEdit } from './OrgInfo';
import PaymentSetup from './PaymentSetup';
import { Invoices } from './Invoices';
import { TaxInfo } from './TaxInfo';
import UserSettings from './UserSettings';
import { AcmeInc } from './AcmeInc';

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  top: {
    backgroundColor: '#7d7d7d',
    marginBottom: 30,
    color: '#fff',
    paddingLeft: 10
  },
  bar: {
    backgroundColor: '#000',
    margin: 30,
    width: 'auto',
    borderRadius: 7
  },
  tab: {
    textTransform: 'initial'
  },
  tabPanel: {
    marginLeft: 50,
    marginRight: 50,
  }
}));

const MyOrgBar = (props) => {
  const { classes, value, setValue } = props;

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <AppBar position="static" className={classes.bar}>
      <Tabs value={value} onChange={handleChange} aria-label="simple tabs example" >
        <Tab label="Org Info" {...a11yProps(0)} className={classes.tab} />
        <Tab label="Payment Setup" {...a11yProps(1)} className={classes.tab} />
        <Tab label="Invoices" {...a11yProps(2)} className={classes.tab} />
        <Tab label="Tax Info" {...a11yProps(3)} className={classes.tab} />
        <Tab label="User Settings" {...a11yProps(4)} className={classes.tab} />
        <Tab label="Acme Inc" {...a11yProps(5)} className={classes.tab} style={{ marginLeft: 'auto' }}/>
      </Tabs>
    </AppBar>
  );
};

export const MyOrgEdit = (props) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  return (
    <div className={classes.root}>
      <Typography variant="h6" className={classes.top}>My Organization</Typography>
      <MyOrgBar classes={classes} value={value} setValue={setValue} />
      <OrgInfoEdit value={value} index={0} className={classes.tabPanel} {...props} />
      <PaymentSetup value={value} index={1} className={classes.tabPanel} {...props} />
      <Invoices value={value} index={2} className={classes.tabPanel} {...props} />
      <TaxInfo value={value} index={3} className={classes.tabPanel} {...props} />
      <UserSettings value={value} index={4} className={classes.tabPanel} {...props} />
      <AcmeInc value={value} index={5} className={classes.tabPanel} {...props} />
    </div>
  );
}