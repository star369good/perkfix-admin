import React from 'react';
// eslint-disable-next-line
import { Create, Edit, SimpleForm, required } from 'react-admin';
import { Typography, Grid } from '@material-ui/core';
import { EditSelectInput, EditTextInput, EditFormLabel, EditRowGrid, EditToolbar } from '../../components';


const OrgInfoEditForm = (props) => {
    const validateOrgInfoUpdate = (values) => {
        const errors = {};
        // if (!values.entity_name) {
        //     errors.entity_name = [{ message: 'ra.validation.number'}];
        // }
        return errors;
    };
   
    return ( <Edit resource={props.resource} id={props.id} basePath={props.path} className={props.className} >
        <SimpleForm toolbar={<EditToolbar onSubmit={props.onSubmit} />} validate={validateOrgInfoUpdate}>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Legal Entity Name</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="entity_name" placeholder="ACME INC" validate={[required()]} fullWidth />
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Entity Type</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditSelectInput source="entity_type" validate={[required()]}
                        choices={[ { id: 'S-Corp', name: 'ra.pages.org.sCorp' },
                        { id: 'T-Corp', name: 'ra.pages.org.tCorp' }, 
                        { id: 'ST-Corp', name: 'ra.pages.org.stCorp' } ]}
                        />
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Website</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="website" placeholder="acme.com" validate={[required()]} fullWidth />
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel >D-U-N-S&reg; Number</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="duns_number" placeholder="xx-xxx-xxxx" fullWidth />
                </Grid>
                <Grid item md={4} sm={6} xs={12} >
                    <Typography component="a" href="http://sample.com">
                        Look up D-U-N-S
                    </Typography>
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel >EIN / Tax ID</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="tax_id" placeholder="012-3456789" fullWidth />
                </Grid>
                <Grid item md={4} sm={6} xs={12} >
                    Either D-U-N-S&reg; Number or valid EIN is required.
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true}>Primary Contact</EditFormLabel>
                </Grid>
                <Grid item md sm={6} xs={12}>
                    <EditTextInput source="primary_firstname" placeholder="First Name" validate={[required()]} fullWidth />
                </Grid>
                <Grid item md sm={6} xs={12}>
                    <EditTextInput source="primary_lastname" placeholder="Last Name" validate={[required()]} fullWidth />
                </Grid>
                <Grid item md={4} sm={6} xs={12} >
                    Should be the business owner, perk budget approver, or a C-level executive.
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Contact Number</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="primary_number" placeholder="(xxx) xxx xxxx" validate={[required()]} fullWidth />
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Contact Email</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="primary_email" validate={[required()]} fullWidth />
                </Grid>
                <Grid item md={4} sm={6} xs={12} >
                    Should be a corporate email.
                </Grid>
            </EditRowGrid>
        </SimpleForm>
    </Edit> );
};

const CustomTypography = ({basePath, ...props}) => (
    <Typography component="h3" style={{ marginTop: 20 }} >{props.children}</Typography>
);
const CustomHr = ({basePath, ...props}) => (
    <hr style={{ width: '100%', marginBottom: 25 }} />
);

const OrgInfoAddressEditForm = (props) => {
    const validateOrgInfoAddressUpdate = (values) => {
        const errors = {};
        if (values.secondary_email === 'secondary@email.com') { // TODO
            errors.secondary_number = [{ message: 'ra.validation.secondaryNotEqualPrimary'}];
        }
        return errors;
    };

    return ( <Edit resource={props.resource} id={props.id} basePath={props.path} className={props.className} >
        <SimpleForm toolbar={<EditToolbar onCancel={props.onCancel} validate={validateOrgInfoAddressUpdate} />} >
            <CustomTypography >Address of your company's HQ</CustomTypography>
            <CustomHr />
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Street</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="street" validate={[required()]} fullWidth />
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel >Unit</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="unit" fullWidth />
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >City</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="city" validate={[required()]} />
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >State</EditFormLabel>
                </Grid>
                <Grid item md sm={6} xs={12}>
                    <EditTextInput source="state" validate={[required()]} fullWidth />
                </Grid>
                <Grid item md sm={4} xs={12}>
                    <EditFormLabel required={true} >Country</EditFormLabel>
                </Grid>
                <Grid item md sm={6} xs={12}>
                    <EditTextInput source="country" validate={[required()]} fullWidth />
                </Grid>
                <Grid item md={4} sm xs >
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Zip</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="zip" validate={[required()]} />
                </Grid>
            </EditRowGrid>
            <CustomHr />
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel >Secondary</EditFormLabel>
                </Grid>
                <Grid item md sm={6} xs={12}>
                    <EditTextInput source="secondary_firstname" placeholder="First Name" fullWidth />
                </Grid>
                <Grid item md sm={6} xs={12}>
                    <EditTextInput source="secondary_lastname" placeholder="Last Name" fullWidth />
                </Grid>
                <Grid item md={4} sm={6} xs={12} >
                    We will reachout to this contact in case we are unable to reach the primary contact.
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Contact Number</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="secondary_number" placeholder="(xxx) xxx xxxx" validate={[required()]} fullWidth />
                </Grid>
            </EditRowGrid>
            <EditRowGrid>
                <Grid item md={3} sm={4} xs={12}>
                    <EditFormLabel required={true} >Contact Email</EditFormLabel>
                </Grid>
                <Grid item md={5} sm={6} xs={12}>
                    <EditTextInput source="secondary_email" validate={[required()]} fullWidth />
                </Grid>
                <Grid item md={4} sm={6} xs={12} >
                    Should be your work email.
                </Grid>
            </EditRowGrid>
        </SimpleForm>
    </Edit> );
};

export const OrgInfoEdit = (props) => {
    const { value, index } = props;
    const [ statusValue, setStatusValue ] = React.useState(0);
    const onSubmit = () => {
        setStatusValue(1);
    };
    const onCancel = () => {
        setStatusValue(0);
    };

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            >
            { value === index && (
                statusValue === 0 ?
             <OrgInfoEditForm {...props} onSubmit={onSubmit} /> :
             <OrgInfoAddressEditForm {...props} onCancel={onCancel} /> )
             }
        </Typography>
    );
};