import React, { Component } from 'react';
import { PlaidLink } from 'react-plaid-link';
import { Typography, Grid } from '@material-ui/core';

class PaymentPlaid extends Component {

    constructor(props) {
        super(props);
    }

    handleOnLoad() {
        // load
    }

    handleOnEvent() {
        // on event
    }

    handleOnSuccess(token, metadata) {
        // send token to client server
    }

    handleOnExit() {
        // handle the case when your user exits Link
    }

    render() {
        const { value, index } = this.props;
        
        return (
            <Typography
                component="div"
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                >
                { value === index && (
                    <PlaidLink
                        clientName="Your app name"
                        env="sandbox"
                        // institution={null}
                        publicKey={process.env.REACT_APP_PLAID_PUBLIC_KEY}
                        product={[ 'transactions']}
                        // apiVersion={'v2'}
                        countryCodes={['US']}
                        token={'public-token-123...'}
                        // selectAccount={true} // deprecated – use https://dashboard.plaid.com/link
                        // webhook="https://webhooks.test.com"
                        onEvent={this.handleOnEvent}
                        onExit={this.handleOnExit}
                        onLoad={this.handleOnLoad}
                        onSuccess={this.handleOnSuccess}>
                        Open Link and connect a bank account to Plaid
                    </PlaidLink> 
                )}
            </Typography>
        );
    }
}

export default PaymentPlaid;