import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

export const AcmeInc = (props) => {
    const { value, index } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            >
            {value === index && <Box p={3}>
                <Typography component="p">
                    Acme Inc
                </Typography>
                </Box>}
        </Typography>
    );
};