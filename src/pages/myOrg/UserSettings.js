import React, { Component } from 'react';
// eslint-disable-next-line
import { Create, Edit, SimpleForm, required, ImageInput, ImageField, FileInput } from 'react-admin';
import { Typography, Grid } from '@material-ui/core';
// import ReactCrop from 'react-image-crop';
import 'react-image-crop/lib/ReactCrop.scss';
import { EditTextInput, EditFormLabel, EditRowGrid, EditToolbar, EditCheckBox, EditImageCropper, EditPasswordDialog } from '../../components';


const CustomHr = ({basePath, ...props}) => (
    <hr style={{ width: '100%', marginBottom: 25 }} />
);
class UserSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showDialog: false
        }
    }
    handleSaveClick = () => {
        this.setState({ showDialog: false });
      }
    handleCloseClick = () => {
       console.log("Hello");
       this.setState({ showDialog: false });
    }
    onChangePasswordClick = e =>{
        e.preventDefault();
        this.setState({
            showDialog: true
        });
    }
    onChangeIDClick() {
        console.log("ID");
    }

    render() {
        const { value, index } = this.props;
        const { showDialog } = this.state;
        const dialogRef = React.createRef();
        
        return (
            <Typography
                component="div"
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                >
                <EditPasswordDialog 
                    submit={(result) => {console.log(result)}} 
                    handleSaveClick={()=>this.handleSaveClick()} 
                    handleCloseClick={()=>this.handleCloseClick()} 
                    showDialog={showDialog}
                />
                { value === index && (
                <Edit resource={this.props.resource} id={this.props.id} basePath={this.props.path} className={this.props.className} >
                    <SimpleForm toolbar={<EditToolbar onCancel={this.props.onCancel} />} >
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12}>
                                <EditFormLabel>My Profile</EditFormLabel>
                            </Grid>
                        </EditRowGrid>
                        <EditRowGrid>
                            <Grid item style={{ marginLeft: '20px' }}>
                                <EditImageCropper ref={dialogRef} source="picture" />
                            </Grid>
                            <Grid item md={6} sm={7} xs={8} style={{ marginLeft: '50px' }}>
                                <div>
                                    <a href="#" onClick={this.onChangePasswordClick} >
                                        Change Password
                                    </a>
                                </div>
                                <div style={{ marginTop: 30 }}>
                                    <a href="/" onClick={this.onChangeIDClick} >
                                        Upload I.D
                                    </a>
                                </div>
                            </Grid>
                        </EditRowGrid>
                        <CustomHr />
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12}>
                                <EditFormLabel required={true}>People Ops Contact</EditFormLabel>
                            </Grid>
                            <Grid item md sm={6} xs={12}>
                                <EditTextInput source="primary_firstname" placeholder="First Name" fullWidth />
                            </Grid>
                            <Grid item md sm={6} xs={12}>
                                <EditTextInput source="primary_lastname" placeholder="Last Name" fullWidth />
                            </Grid>
                            <Grid item md={4} sm={6} xs={12} >
                                Resource who will actively manage or over see the perk program (HR/ People Ops/ Team lead).
                            </Grid>
                        </EditRowGrid>
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12}>
                                <EditFormLabel required={true}>Contact Number</EditFormLabel>
                            </Grid>
                            <Grid item md={5} sm={6} xs={12}>
                                <EditTextInput source="primary_number" fullWidth />
                            </Grid>
                        </EditRowGrid>
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12}>
                                <EditFormLabel required={true}>Contact Email</EditFormLabel>
                            </Grid>
                            <Grid item md={5} sm={6} xs={12}>
                                <EditTextInput source="primary_email" validate={[required()]} fullWidth />
                            </Grid>
                        </EditRowGrid>
                        <CustomHr />
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12}>
                                <EditCheckBox name="perk_budget_approver">Add Perk Budget Approver</EditCheckBox>
                            </Grid>
                        </EditRowGrid>
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12}>
                            </Grid>    
                            <Grid item md sm={6} xs={12}>
                                <EditTextInput source="secondary_firstname" placeholder="First Name" fullWidth />
                            </Grid>
                            <Grid item md sm={6} xs={12}>
                                <EditTextInput source="secondary_lastname" placeholder="Last Name" fullWidth />
                            </Grid>
                            <Grid item md={4} sm={6} xs={12} >
                                This contact’s email can be used to request perk budget approval.
                            </Grid>
                        </EditRowGrid>
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12}>
                                <EditFormLabel>Contact Number</EditFormLabel>
                            </Grid>
                            <Grid item md={5} sm={6} xs={12}>
                                <EditTextInput source="secondary_number" placeholder="(xxx) xxx xxxx" validate={[required()]} fullWidth />
                            </Grid>
                        </EditRowGrid>
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12}>
                                <EditFormLabel>Contact Email</EditFormLabel>
                            </Grid>
                            <Grid item md={5} sm={6} xs={12}>
                                <EditTextInput source="secondary_email" validate={[required()]} fullWidth />
                            </Grid>
                        </EditRowGrid>
                    </SimpleForm>
                </Edit>)
                 }
            </Typography>
        );
    }
}
export default UserSettings;
