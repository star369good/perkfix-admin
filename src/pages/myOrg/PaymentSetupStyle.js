const PaymentSetupStyle = theme => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
    },
    inactiveNav: {
        width: '153px',
        height: '57px',
        border: '1px solid #757575',
        color: '#757575',
        textDecoration: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '6px',
        fontSize: '1.3rem',
        marginRight: '30px',
    },
    activeNav: {
        color: '#0091FF'
    },
    button: {
        textTransform: 'initial',
        boxShadow: 'inset 0px 1px 0px 0px #ffffff',
        background: 'linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%)',
        backgroundColor: '#f9f9f9',
        borderRadius: 6,
        border: '1px solid #dcdcdc',
        fontSize: '1.2em',
        marginRight: '15px',
        color: '#000',
    },
    button_active: {
        color: '#0091FF',
        fontWeight: 'Bold'
    },
});


export default PaymentSetupStyle;