import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Typography, Grid, withStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { Edit, SimpleForm, required, Button } from 'react-admin';
import { Captcha, captchaSettings } from 'reactjs-captcha';
import PaymentSetupStyle from './PaymentSetupStyle';
import { EditTextInput, EditFormLabel, EditRowGrid, EditToolbar, EditCheckBox, EditSelectInput } from '../../components';


const apiBaseUrl = process.env.REACT_APP_API_BASEURL;

const CustomHr = ({basePath, ...props}) => (
    <hr style={{ width: '97%', marginBottom: 25 }} />
);

class PaymentSetup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active_flag: true,
            captcha_error_flag: false
        };
        captchaSettings.set({
            captchaEndpoint: 
            apiBaseUrl + '/wp-content/recaptcha/botdetect-captcha-lib/simple-botdetect.php'
        });
        this.onClickCaptcha = this.FormWithCaptchaOnSubmitHandler.bind(this);
    }

    setActive(flag) {
        this.setState({
            active_flag: flag
         });
    }

    validatePaymentUpdate(values) {
        const errors = {};

        // errors.entity_type = [{ message: 'ra.validation.number'}];

        return errors;
    }

     // process the yourFormWithCaptcha on submit event
    FormWithCaptchaOnSubmitHandler(event) {

    // the user-entered captcha code value to be validated at the backend side
    let userEnteredCaptchaCode = this.captcha.getUserEnteredCaptchaCode();

    // the id of a captcha instance that the user tried to solve
    let captchaId = this.captcha.getCaptchaId();

    let postData = {
      "userEnteredCaptchaCode": userEnteredCaptchaCode,
      "captchaId": captchaId
    };

    let self = this;

    // post the captcha data to the /your-app-backend-path on your backend.
    // make sure you import the axios in this view with: import axios from 'axios';
    fetch(apiBaseUrl + '/wp-content/recaptcha/index.php', {
      method: 'post',
      body: JSON.stringify(postData),
      } 
     )
    .then(response => response.json())
    .then(data => {
        if (data.success === false) {
            // captcha validation failed; reload image
            // self.captcha.reloadImage();
            // TODO: maybe display an error message, too
            this.setState({
                captcha_error_flag: true
             });
             console.log(event);
            return false;
        } else {
            // TODO: captcha validation succeeded; proceed with your workflow
            this.setState({
                captcha_error_flag: false
             });
            console.log("succeed");
            return true;
        }
    })
    .catch(err => {
      console.log(err);
    });
    event.preventDefault();
    return false;
   }

    render() {
        const { value, index, classes } = this.props;
        const { active_flag, captcha_error_flag } = this.state;

        return (
            <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            >
            {value === index && <Box p={3}>
                <Edit resource={this.props.resource} id={this.props.id} basePath={this.props.path} className={this.props.className} >
                    <SimpleForm toolbar={<EditToolbar onCancel={this.props.onCancel} onSubmit={this.onClickCaptcha} saveButtonLabel="ra.action.next" />} validate={this.validatePaymentUpdate} >
                        <EditRowGrid>
                            <Grid item md={5} sm={4} xs={12}>
                                <EditFormLabel style={{ color: 'rgba(0,0,0,0.72)' }}>Choose payment method to fund perk program</    EditFormLabel>
                            </Grid>
                        </EditRowGrid>
                        <CustomHr />
                        <EditRowGrid>
                            <Grid item md={3} sm={4} xs={12} style={{ marginBottom: '20px'}}>
                                <EditFormLabel>Payment Type</EditFormLabel>
                            </Grid>
                            <Grid item md={5} sm={6} xs={12} style={{ marginBottom: '20px'}}>
                                <Button key="ACH(eCheck)" label="ra.action.ach_btn" size="large" redirect={false} submitOnEnter={true} className={`${classes.button} ${active_flag && classes.button_active}`} onClick={() => this.setActive(true)}  />
                                <Button key="Debit/Credit" label="ra.action.debit_btn" size="large" redirect={false} submitOnEnter={true} className={`${classes.button} ${!active_flag && classes.button_active}`} onClick={() => this.setActive(false)} />
                            </Grid>
                        </EditRowGrid>

                        {active_flag && (<>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}>
                                    <EditFormLabel>Account Type</EditFormLabel>
                                </Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <EditSelectInput source="entity_type" validate={[required()]}
                                        choices={[ { id: 'S-Corp', name: 'ra.pages.org.checkingAccount' },
                                        { id: 'T-Corp', name: 'ra.pages.org.savingsAccount' }, 
                                        ]}
                                        />
                                </Grid>
                            </EditRowGrid>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}>
                                    <EditFormLabel>Routing Number</EditFormLabel>
                                </Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <EditTextInput source="primary_number" fullWidth />
                                </Grid>
                            </EditRowGrid>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}>
                                    <EditFormLabel>Account Number</EditFormLabel>
                                </Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <EditTextInput source="primary_email" validate={[required()]} fullWidth />
                                </Grid>
                            </EditRowGrid>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}>
                                    <EditFormLabel>Confirm Account Number</EditFormLabel>
                                </Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <EditTextInput source="primary_email" validate={[required()]} fullWidth />
                                </Grid>
                            </EditRowGrid>
                        </>)}
                        {!active_flag && (<>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}>
                                    <EditFormLabel>Account Type</EditFormLabel>
                                </Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <EditSelectInput source="entity_type" validate={[required()]}
                                        choices={[ { id: 'S-Corp', name: 'ra.pages.org.sCorp' },
                                        { id: 'T-Corp', name: 'ra.pages.org.tCorp' }, 
                                        { id: 'ST-Corp', name: 'ra.pages.org.stCorp' } ]}
                                        />
                                </Grid>
                            </EditRowGrid>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}>
                                    <EditFormLabel>Routing Number</EditFormLabel>
                                </Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <EditTextInput source="primary_number" fullWidth />
                                </Grid>
                            </EditRowGrid>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}>
                                    <EditFormLabel>Account Number</EditFormLabel>
                                </Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <EditTextInput source="primary_email" validate={[required()]} fullWidth />
                                </Grid>
                            </EditRowGrid>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}>
                                    <EditFormLabel>Confirm Account Number</EditFormLabel>
                                </Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <EditTextInput source="primary_email" validate={[required()]} fullWidth />
                                </Grid>
                            </EditRowGrid>
                           
                        </>)}
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12} style={{paddingLeft: "30px", paddingTop: "20px", color: "rgba(0, 0, 0, 0.5)"}}>
                                    Enter the characters in the image below
                                </Grid>    
                            </EditRowGrid>
                            <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12} style={{paddingLeft: "30px"}}>
                                    <Captcha captchaStyleName="paymentFormWithCaptcha" ref={(captcha) => {this.captcha = captcha}} />   
                                </Grid>    
                                <Grid item md={5} sm={6} xs={12}>
                                    <input type="text" id="perkcaptcha_input" style={{textTransform: "uppercase", border: "1px solid #ccc", borderRadius: "5px", padding: "11px 20px", fontSize: "16px"}}/>
                                </Grid>
                            </EditRowGrid>
                            {captcha_error_flag === true ? <EditRowGrid>
                                <Grid item md={3} sm={4} xs={12}></Grid>
                                <Grid item md={5} sm={6} xs={12}>
                                    <label style={{color: "red"}}>The entered characters don't match with the characters in the image</label>
                                </Grid>
                            </EditRowGrid> : ""
                            }
                            
                            <EditRowGrid>
                                <Grid item md={10} sm={4} xs={12} style={{paddingTop: "30px"}}>
                                    <EditCheckBox name="perk_budget_approver">I accept <EditFormLabel required={true} style={{paddingLeft: "5px", fontSize: "1.0em"}}><Link to="/" style={{color: 'rgba(0, 145, 255, 1)'}}>terms and conditions.</Link></EditFormLabel></EditCheckBox>
                                </Grid>
                            </EditRowGrid>

                    </SimpleForm>
                </Edit>
            </Box>}
        </Typography>
        );
    }
}

export default withStyles(PaymentSetupStyle)(PaymentSetup)



