import React from 'react';
import { Admin, Resource } from 'react-admin';

import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import TuneOutlinedIcon from '@material-ui/icons/TuneOutlined';
import HouseOutlinedIcon from '@material-ui/icons/HouseOutlined';
import BuildOutlinedIcon from '@material-ui/icons/BuildOutlined';
import ReportOutlinedIcon from '@material-ui/icons/ReportOutlined';
import ForumOutlinedIcon from '@material-ui/icons/ForumOutlined';
import CardGiftcardOutlinedIcon from '@material-ui/icons/CardGiftcardOutlined';
import PeopleOutlineOutlinedIcon from '@material-ui/icons/PeopleOutlineOutlined';
import BrightnessAutoOutlinedIcon from '@material-ui/icons/BrightnessAutoOutlined';
import SettingsIcon from '@material-ui/icons/Settings';
import InfoIcon from '@material-ui/icons/Info';

import { AdminLayout } from './components';
import { Automation, Faq, Home, Inbox, MyOrgEdit, NoFound, Perks, Reports, Settings, Setup, Teams, Tools } from './pages';

import dataProvider from './services/dataProvider';
import i18nProvider from './i18n/i18nProvider';

const App = () => {
  const userId = localStorage.getItem('perkfix_user_id');
  const teamId = localStorage.getItem('perkfix_team_id');
  const orgId = localStorage.getItem('perkfix_org_id');

  return (userId && teamId && orgId && 
    <Admin 
      dataProvider={dataProvider}
      layout={AdminLayout}
      catchAll={NoFound}
      title="Perkfix Admin"
      dashboard={Home}
      i18nProvider={i18nProvider}
      >
        <Resource name="home" list={Home} icon={HomeOutlinedIcon} options={{ label: 'Home' }}/>
        <Resource name="setup" list={Setup} icon={TuneOutlinedIcon} options={{ label: 'Setup' }}/>
        <Resource name="org" edit={MyOrgEdit} icon={HouseOutlinedIcon} options={{ label: 'My Org', url: '/'+orgId }} />
        <Resource name="tool" list={Tools} icon={BuildOutlinedIcon} options={{ label: 'Tools' }}/>
        <Resource name="report" list={Reports} icon={ReportOutlinedIcon} options={{ label: 'Reports' }}/>
        <Resource name="inbox" list={Inbox} icon={ForumOutlinedIcon} options={{ label: 'Inbox' }}/>
        <Resource name="perk" list={Perks} icon={CardGiftcardOutlinedIcon} options={{ label: 'Perks' }}/>
        <Resource name="team" list={Teams} icon={PeopleOutlineOutlinedIcon} options={{ label: 'Teams' }}/>
        <Resource name="automation" list={Automation} icon={BrightnessAutoOutlinedIcon} options={{ label: 'Automation' }}/>
        <Resource name="setting" list={Settings} icon={SettingsIcon} options={{ label: 'Settings' }}/>
        <Resource name="faq" list={Faq} icon={InfoIcon} options={{ label: 'FAQ' }}/>
    </Admin>
  );
};

export default App;